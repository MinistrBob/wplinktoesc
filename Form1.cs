﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Web;
using System.Diagnostics;
using System.Net;
using System.IO;
using System.Xml;

namespace WPLinkToESC
{
	public partial class Form1 : Form
	{
		const string metka = @"В это поле нужно drag&drop или copy\past URL";
		const string google_translater = @"http://translate.google.ru/translate?sl=ru&tl=en&u=";
		
		public Form1()
		{
			InitializeComponent();
			textBox1.Text=metka;
		}

		private void ResizeForm()
		{
			Size size = new Size();
			Screen scrn = Screen.FromControl(this);
			if (textBox1.Text=="" || textBox1.Text==metka)
			{
			   size.Width=800;
               textBox2.Text = String.Empty;
               textBox3.Text = String.Empty;
               textBox4.Text = String.Empty;
			}
			else
			{
				if (textBox1.Text.Length > textBox2.Text.Length)
				{
					size = TextRenderer.MeasureText(textBox1.Text, textBox1.Font);  
				}
				else
				{
					size = TextRenderer.MeasureText(textBox2.Text, textBox2.Font);
				}
			}
			
			if (size.Width > scrn.Bounds.Width)
			{
				this.Location = new Point(0, this.Location.Y);
				this.Width = scrn.Bounds.Width;
			}
			else
			{
				this.Width = size.Width + 70;
			}

		}

		private string ReplaceSpecialSymbols(string str)
		{
			str = str.Replace(":", "%3A").Replace("/", "%2F");
			return str;
		}

		private void textBox1_TextChanged(object sender, EventArgs e)
		{

			string str = textBox1.Text;

			str = HttpUtility.UrlDecode(str);

            // Удаляю замыкающий слэш если он есть
            if (str.Substring(str.Length-1,1)=="/")
            {
                str = str.Substring(0, str.Length - 1); 
            }

			textBox4.Text = str.Substring(str.LastIndexOf('/', str.Length-1) + 1, str.Length - str.LastIndexOf('/', str.Length - 1)-1)
				.Replace("-"," ");
			textBox2.Text = google_translater + ReplaceSpecialSymbols(str);
            if (checkBox1.Checked)
            {
                linkLabel4LinkClicked();
            }
			
			ResizeForm();
		}

		private void textBox1_DragEnter(object sender, DragEventArgs e)
		{
			if (e.Data.GetDataPresent(DataFormats.UnicodeText))
			{
				textBox1.Text = (string)e.Data.GetData(DataFormats.UnicodeText); ;
			}
			else e.Effect = DragDropEffects.None;
		}

		private void exitToolStripMenuItem_Click(object sender, EventArgs e)
		{
			Application.Exit();
		}

		private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
		{
			Process.Start(textBox2.Text);
		}

		private void Form1_LocationChanged(object sender, EventArgs e)
		{
			ResizeForm();
		}

		private void linkLabel2_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
		{
			textBox3.SelectAll();
			textBox3.Copy();
		}

		private void linkLabel3_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
		{
			textBox2.SelectAll();
			textBox2.Copy();
		}

		//private void linkLabel4_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
		//{
		//    //HttpWebRequest request = (HttpWebRequest)WebRequest.Create
		//    //         ("http://translate.yandex.net/api/v1/tr/translate?lang=en-ru&text=To+be,+or+not+to+be%3F&text=That+is+the+question.");

		//    //request.UserAgent = "Mozilla/5.0";
		//    ////request.AllowAutoRedirect = true;
		//    ////request.Referer = "http://mysite.ru/";
		//    //HttpWebResponse response = (HttpWebResponse)request.GetResponse();

		//    //string html = new StreamReader(response.GetResponseStream(),
		//    //                               Encoding.UTF8).ReadToEnd();

		//    XmlTextReader reader = new XmlTextReader("http://translate.yandex.net/api/v1/tr/translate?lang=en-ru&text=To+be,+or+not+to+be%3F&text=That+is+the+question.");

		//    // Skip non-significant whitespace  
		//    reader.WhitespaceHandling = WhitespaceHandling.Significant;

		//    // Read nodes one at a time  
		//    string temp1;
		//    string temp2;
		//    string temp3;
		//    while (reader.Read())
		//    {
		//        temp1 = String.Format("NodeType={0}||Name={1}||Value={2}\n", reader.NodeType, reader.Name, reader.Value);
		//        textBox4.AppendText(temp1);
		//        if (reader.NodeType.ToString() == "Text") { textBox3.Text = reader.Value; }
		//    }  

		//}

		private void linkLabel4_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
		{
            linkLabel4LinkClicked();
		}

        private void linkLabel4LinkClicked()
        {
            textBox3.Text = MyTranslater(textBox4.Text).Replace(" ", "-").ToLower();
            // Очистка строки
            textBox3.Text = textBox3.Text.Replace(".", "-").Replace(",", "-");
            textBox3.Text = textBox3.Text.Replace("-a-", "-").Replace("-the-", "-").Replace("-to-", "-").Replace("-of-", "-");
            // Окончательная зачистка
            textBox3.Text = textBox3.Text.Replace("--", "-");
        }

		private string MyTranslater (string str)
		{
            XmlTextReader reader = new XmlTextReader("https://translate.yandex.net/api/v1.5/tr/translate?key=trnsl.1.1.20130801T131139Z.d52bd55ff3fd7e7d.04e7f5cfe56e16995a8d37df1ce01054e6f1122e&lang=ru-en&text=" + str);

			// Skip non-significant whitespace  
			//reader.WhitespaceHandling = WhitespaceHandling.Significant;

			// Read nodes one at a time  

			while (reader.Read())
			{
				if (reader.NodeType.ToString() == "Text") { str = reader.Value; }
			}
			return str;
		}

        private void Form1_Load(object sender, EventArgs e)
        {
            checkBox1.Checked = Properties.Settings.Default.AutoTranslate;
            textBox1.Font = Properties.Settings.Default.AppFont;
            textBox2.Font = Properties.Settings.Default.AppFont;
            textBox3.Font = Properties.Settings.Default.AppFont;
            textBox4.Font = Properties.Settings.Default.AppFont;
            //AssignAllControlsContextMenu(this.Controls);
            textBox1.ContextMenuStrip = contextMenuStrip1;
            textBox2.ContextMenuStrip = contextMenuStrip1;
            textBox3.ContextMenuStrip = contextMenuStrip1;
            textBox4.ContextMenuStrip = contextMenuStrip1;
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            Properties.Settings.Default.AutoTranslate = checkBox1.Checked;
            Properties.Settings.Default.Save();
        }

        private void fontToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ChangeFont();
        }

        private void fontToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            ChangeFont();
        }

        private void ChangeFont()
        {
            if (fontDialog1.ShowDialog() == DialogResult.OK)
            {
                textBox1.Font = fontDialog1.Font;
                textBox2.Font = fontDialog1.Font;
                textBox3.Font = fontDialog1.Font;
                textBox4.Font = fontDialog1.Font;
                Properties.Settings.Default.AppFont = fontDialog1.Font;
                Properties.Settings.Default.Save();
            }
            
        }

        private void Form1_SizeChanged(object sender, EventArgs e)
        {
            Size fsize = new Size();
            fsize = this.Size;

        }

        //public void AssignAllControlsContextMenu (Control.ControlCollection controls)
        //{
        //    foreach (Control ctl in controls)
        //    {
        //        //if (ctl is UserControl)
        //        //{
        //            ctl.ContextMenuStrip = contextMenuStrip1;
        //        //}

        //        if (ctl.Controls.Count > 0)
        //            AssignAllControlsContextMenu(ctl.Controls);
        //    }
        //}

	}
}
